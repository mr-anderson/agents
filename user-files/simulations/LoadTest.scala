package johanvo.gatling.simulations

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import scala.util.Random

class LoadTest extends Simulation {
  var baseUrl = System.getProperty("base-url")

  val httpProtocol = Setup.protocol
    .baseUrl(baseUrl)

  val agents = scenario("Agents")
    .feed(Feeders.randomNr)
    .exec(Agent.byAgentId)

  setUp(
    agents.inject(rampUsers(1000) during (20 seconds)),
  ).protocols(httpProtocol)
}
