package johanvo.gatling.simulations

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object Setup {
  val protocol = http
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")
}

object Agent {
  val byAgentId = exec(http("byAgentId")
    .get("/agent/${id}"))
}

object Feeders {
  val randomNr = Iterator.continually(
    Map("id" -> (scala.util.Random.nextInt(999)))
  )
}
