#!/bin/bash

export GATLING="/home/anarril/Applications/gatling-charts-highcharts-bundle-3.0.3/bin/gatling.sh"
export BASE_URL=$1
export JAVA_OPTS="-Dbase-url=http://${BASE_URL}"

${GATLING} \
	--run-description ${BASE_URL} \
	--simulation johanvo.gatling.simulations.LoadTest

./open-most-recent.sh
