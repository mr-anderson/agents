#!/bin/bash

# automatically open the most recently generated report
ls -t1 results/ \
    | head -n 1 \
    | awk '{print "results/"$1"/index.html"}' \
    | xargs firefox --new-tab;
